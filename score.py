# coding=utf8

"""SIGHAN Evaluation for Word Segment.

Usage:
  score.py  <dictionary> <truth> <test> <output_file>
  score.py  (-h | --help)
  score.py  --version

Options:
  -h --help     Show this screen.
  -v --version     Show version.

"""

import logging
import os
import re
import subprocess
from subprocess import PIPE

from docopt import docopt

logging.basicConfig(level=logging.DEBUG, filename='log')

CHARSET = 'utf8'


def read_file(path):
    """read a file."""
    if os.path.isfile(path):
        with open(path, 'r') as corpus:
            return corpus.read().decode(CHARSET)
    else:
        print "{} not found.".format(path)
        exit(1)


def write_file(path, context):
    """ write a file."""
    with open(path, 'w') as corpus:
        corpus.write(context.encode(CHARSET))


def main():
    """create a report."""
    argments = docopt(__doc__, version="sighan evaluation 1.0")
    dict_file = argments["<dictionary>"]
    truth_file = argments["<truth>"]
    test_file = argments["<test>"]
    output_file = argments["<output_file>"]

    dict_corpus = read_file(dict_file)
    word_dict = {}
    for line in dict_corpus.split('\n'):
        line = line.strip()
        line = re.sub(r'^\s*', '', line)
        line = re.sub(r'\s*$', '', line)
        word_dict[line] = 1

    total = all_dels = all_ins = all_subst = all_true_count = all_test_count = 0
    all_raw_recall = all_raw_precision = 0

    iv_missed = oov_missed = oov = iv = 0

    truth_corpus = read_file(truth_file).split('\n')
    test_corpus = read_file(test_file).split('\n')

    linenum = 0
    score_info = ''
    while linenum < len(truth_corpus) and linenum < len(test_corpus):
        truth_line = re.sub(r'^\s*', '', truth_corpus[linenum])
        test_line = re.sub(r'^\s*', '', test_corpus[linenum])
        truth_line = re.sub(r'\s*$', '', truth_line)
        test_line = re.sub(r'\s*$', '', test_line)
        truth_line = re.sub(r'\n', '', truth_line)
        test_line = re.sub(r'\n', '', test_line)

        truth_words = truth_line.split(' ')
        test_words = test_line.split(' ')

        truth_count = len(truth_words)
        test_count = len(test_words)

        info = u"{}'s word size: {}".format(truth_line, str(truth_count))
        logging.info(info)

        if truth_count > 0 and test_count == 0:
            err = "Warning: training is 0 but test is nonzero\
                , possible misalignment at line {}.\n".format(str(linenum))
            logging.error(err)

        if test_count == 0:
            err = "Warning: No output in test data where \
                there is in training data, line $linenum\n".format(str(linenum))
            logging.error(err)

        write_file('tmp1', '\n'.join(truth_words))
        write_file('tmp2', '\n'.join(test_words))

        cmd = ['diff', '-y', 'tmp1', 'tmp2']
        process = subprocess.Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()
        logging.error(stderr)

        dels = 0
        ins = 0
        subst = 0
        raw_recall = 0
        raw_precision = 0

        stdout = stdout.split('\n')
        for line in stdout:
            if re.match(r'.*\s+\|.*', line):
                subst += 1

            elif re.match(r'.*\s+\>.*', line):
                ins += 1

            elif re.match(r'.*\s+\<.*', line):
                dels += 1

            if re.search(r'^([^\s]+)', line):
                word = re.search(r'^([^\s]+)', line).group()
                if word not in word_dict:
                    oov += 1
                else:
                    iv += 1

                if re.search(r'^[^\s]+\s.*\s[\|\>\<]\s', line):
                    word = re.search(r'^[^\s]+\s.*\s[\|\>\<]\s', line).group()
                    word = re.search(r'^([^\s]+)', line).group()
                    if word not in word_dict:
                        oov_missed += 1
                    else:
                        iv_missed += 1
                    raw_recall += 1

            if re.search(r'[\|\>\<]', line):
                word = re.search(r'[\|\>\<]', line).group()
                raw_precision += 1

        linenum += 1
        score_info += "--{}-------{}----{}\n".format(truth_file, test_file, str(linenum))
        tot = dels + ins + subst
        total += tot
        all_dels += dels
        all_ins += ins
        all_subst += subst
        all_true_count += truth_count
        all_test_count += test_count
        neg2zero = lambda x: 0 if x < 0 else x
        raw_recall = neg2zero(truth_count - raw_recall)
        raw_precision = neg2zero(test_count - raw_precision)
        all_raw_recall += raw_recall
        all_raw_precision += raw_precision
        raw_recall = float(raw_recall) / truth_count
        raw_precision = float(raw_precision) / test_count
        score_info += ''.join(stdout)+'\n'
        score_info += "INSERTIONS:\t{}\n".format(ins)
        score_info += "DELETIONS:\t{}\n".format(dels)
        score_info += "SUBSTITUTIONS:\t{}\n".format(subst)
        score_info += "NCHANGE:\t{}\n".format(tot)
        score_info += "NTRUTH:\t{}\n".format(truth_count)
        score_info += "NTEST:\t{}\n".format(test_count)
        score_info += "TRUE WORDS RECALL:\t{:2.3f}\n".format(round(raw_recall, 3))
        score_info += "TEST WORDS PRECISION:\t{:2.3f}\n".format(round(raw_precision, 3))

    os.unlink('tmp1')
    os.unlink('tmp2')

    score_info += "=== SUMMARY:\n"
    score_info += "=== TOTAL INSERTIONS:\t{}\n".format(all_ins)
    score_info += "=== TOTAL DELETIONS:\t{}\n".format(all_dels)
    score_info += "=== TOTAL SUBSTITUTIONS:\t{}\n".format(all_subst)
    score_info += "=== TOTAL NCHANGE:\t{}\n".format(total)
    score_info += "=== TOTAL TRUE WORD COUNT:\t{}\n".format(all_true_count)
    score_info += "=== TOTAL TEST WORD COUNT:\t{}\n".format(all_test_count)

    all_raw_recall = float(all_raw_recall) / all_true_count
    all_raw_precision = float(all_raw_precision) / all_test_count
    beta = 1
    recall = all_raw_recall
    precision = all_raw_precision
    f_score = (1 + beta) * precision * recall / (beta * precision + recall)

    score_info += "=== TOTAL TRUE WORDS RECALL:\t{:2.3f}\n".format(round(recall, 3))
    score_info += "=== TOTAL TEST WORDS PRECISION:\t{:2.3f}\n".format(round(precision, 3))
    score_info += "=== F MEASURE:\t{:2.3f}\n".format(round(f_score, 3))

    if oov > 0:
        oov_missed = 1 - oov_missed / oov
        oov_missed = "{:2.3f}".format(round(oov_missed, 3))
    else:
        oov_missed = "--"

    if iv > 0:
        iv_missed = 1 - iv_missed / iv
        iv_missed = "{:2.3f}".format(round(iv_missed, 3))
    else:
        iv_missed = "--"

    score_info += "=== OOV Rate:\t{}\n".format(oov)
    score_info += "=== OOV Recall Rate:\t{}\n".format(oov_missed)
    score_info += "=== IV Recall Rate:\t{}\n".format(iv_missed)

    write_file(output_file, score_info.decode('utf8'))

if __name__ == '__main__':
    main()
    print __file__, 'finish.'
