# coding=utf8

"""Preprocessing for Word Segmentaion.

Usage:
	preprocessing.py dict_tag <text_file> <output_file>
    preprocessing.py split_sent <text_file> <output_file>
    preprocessing.py time_tag <text_file> <output_file>
    preprocessing.py quantifier_tag <text_file> <output_file>
    preprocessing.py all_preprocess <text_file> <output_file>
    preprocessing.py re_train <text_file> <output_file>
    preprocessing.py (-h|--help)
    preprocessing.py --version

Options:
    -h --help   show this screen
    --verion show this version

"""

import os
import re
import json
import logging
import subprocess
from subprocess import PIPE

from docopt import docopt

CHARSET = 'utf8'
SPLIT_SENT_PATH = 'split_sent_crf'
RULE_TAG_TABLE = {}
RULE_TAG_TABLE['time'] = 'T'
RULE_TAG_TABLE['quantifier'] = 'Q'
RULE_FILES_PATH = 'rule'
DICT_TAG_PATH = 'dict'
logging.basicConfig(level=logging.DEBUG, filename="log")


def read_file(path):
    """ read a file. """
    if os.path.exists(path):
        with open(path, 'r') as data:
            return data.read().decode(CHARSET)
    else:
        print "{} could not found".format(path)
        exit(1)


def write_file(path, context):
    """ write a file. """
    context = context.encode(CHARSET)
    with open(path, 'w') as data:
        data.write(context)


def build_tag_dict(tag, tag_data):
    """ use letter and number to represent each name. """
    tag_data = filter(None, tag_data.split('\n'))
    tag_dict = {}
    for index, name in enumerate(tag_data):
        tag_dict[name] = tag + str(index)
    return tag_dict


def dict_tag_corpus(corpus, tag_len=2, tag_freq=1, tag_entropy=1, use_dict_priority='NOPW'):
    """ use tagged dictinary to tag corpus.
    tag_len, tag_freq and tag_entropy are used to filter dict
    """

    dict_file_list = os.listdir(DICT_TAG_PATH)
    dict_file_list = [dict_file for dict_file in dict_file_list if dict_file[0] in use_dict_priority]
    dict_file_list_sorted = sorted(dict_file_list, key=lambda x: use_dict_priority.index(x[0]))

    for dict_file in dict_file_list_sorted:
        tag = os.path.basename(dict_file)[0]
        if tag in use_dict_priority:
            print tag, 'taging...'
            dict_path = os.path.join(DICT_TAG_PATH, dict_file)
            tag_data = read_file(dict_path)
            tag_dict = build_tag_dict(tag, tag_data)
            for name in sorted(tag_dict, reverse=True):
                if len(name) >= tag_len and tag != 'A':
                    tag_no = tag_dict[name]
                    name = re.escape(name)
                    corpus = re.sub(name, '@' + tag_no + '@', corpus)
                elif tag == 'A':
                    print name.encode(CHARSET)
                    tag_no = tag_dict[name]
                    name = name.replace(' ', '')
                    corpus = corpus.replace(name, '@' + tag_no + '@')

    return corpus


def split_sent(paragraph):
    """ use crf to split sentence. """
    tool_path = os.path.join('split_sent_crf', 'Split_sentence.exe')

    input_path = os.path.join(SPLIT_SENT_PATH, 'input_tmp_file')
    input_path = os.path.abspath(input_path)
    write_file(input_path, paragraph)

    output_path = os.path.join(SPLIT_SENT_PATH, 'output_tmp_file')
    output_path = os.path.abspath(output_path)
    cmd = [tool_path]
    split_process = subprocess.Popen(
        cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE, cwd=SPLIT_SENT_PATH)
    stdout, stderr = split_process.communicate(
        input=input_path + '\n' + output_path + '\n')
    if len(stderr) > 0:
        os.unlink(input_path)
        os.unlink(output_path)
        logging.error('split_sent_error')
        exit(1)
    sent_corpus = read_file(output_path)
    sent_corpus = filter(None, sent_corpus.split('\n'))
    corpus = ''
    for sent in sent_corpus:
        sent_box = filter(None, sent.split(' '))
        sent = '\n'.join(sent_box)
        corpus += sent + '\n\n'
    os.unlink(input_path)
    os.unlink(output_path)
    return corpus


def rule_tag_dict(rule_list, corpus, tag_sign):
    """ use rule to tag NE. """
    pat = u'|'.join(rule_list)
    time_pat = re.compile(pat, re.UNICODE)
    line_list = corpus.split('\n')
    new_corpus = ''
    for line in line_list:
        new_line = []
        index = 0
        while index < len(line):
            match_str = ''
            for match_obj in time_pat.finditer(line):
                if index >= match_obj.start() and index < match_obj.end():
                    match_str = tag_sign + match_obj.group() + tag_sign
                    index += len(match_obj.group())
                    break
            if match_str != '':
                new_line.append(match_str)
            else:
                new_line.append(line[index])
                index += 1
        new_line = ''.join(new_line)
        new_line = re.sub(tag_sign + r'\s?' + tag_sign, '', new_line)
        new_corpus += new_line + '\n'

    return new_corpus


def ne_encode(corpus, tag_sign):
    """ time encode """
    ne_pat = re.compile('{}[^{}]+{}'.format(tag_sign,
                                            tag_sign, tag_sign), re.MULTILINE)
    ne_set = set(ne_pat.findall(corpus))
    ne_dict = {}
    dict_count = 0
    for ne in ne_set:
        if ne not in ne_dict:
            dict_count += 1
            ne_dict[ne] = tag_sign + str(dict_count) + tag_sign

    ne_dict_json = json.dumps(ne_dict)
    write_file(tag_sign, ne_dict_json)

    for ne in ne_dict:
        corpus = corpus.replace(ne, ne_dict[ne])

    return corpus


def main():
    """ contral preprocess unit. """
    arguments = docopt(__doc__, version="word segmentation preprocessing 1.0")
    corpus_path = os.path.abspath(arguments['<text_file>'])
    output_path = os.path.abspath(arguments['<output_file>'])
    corpus = read_file(corpus_path)

    if arguments['dict_tag']:
        tagged_corpus = dict_tag_corpus(corpus)
        write_file(output_path, tagged_corpus)

    if arguments['split_sent']:
        sent_corpus = split_sent(corpus)
        write_file(output_path, sent_corpus)

    if arguments['time_tag']:
        rule_path = os.path.join(RULE_FILES_PATH, 'time.rule')
        rule_list = read_file(rule_path).split('\n')
        time_tag_corpus = rule_tag_dict(
            rule_list, corpus, RULE_TAG_TABLE['time'])
        write_file(output_path, time_tag_corpus)

    if arguments['quantifier_tag']:
        rule_path = os.path.join(RULE_FILES_PATH, 'quantifier.rule')
        rule_list = read_file(rule_path).split('\n')
        quantifier_tag_corpus = rule_tag_dict(
            rule_list, corpus, RULE_TAG_TABLE['quantifier'])
        write_file(output_path, quantifier_tag_corpus)

    if arguments['all_preprocess']:

        # use crf split sentence
        sent_corpus = split_sent(corpus)

        # dict tagging
        dict_tagged_corpus = dict_tag_corpus(sent_corpus, use_dict_priority='NOPW')

        # time tagging
        rule_path = os.path.join(RULE_FILES_PATH, 'time.rule')
        rule_list = read_file(rule_path).split('\n')
        time_tagged_corpus = rule_tag_dict(
            rule_list, dict_tagged_corpus, RULE_TAG_TABLE['time'])

        # time encoding
        time_encoded_corpus = ne_encode(
            time_tagged_corpus, RULE_TAG_TABLE['time'])

        # quantifier tagging
        rule_path = os.path.join(RULE_FILES_PATH, 'quantifier.rule')
        rule_list = read_file(rule_path).split('\n')
        quantifier_tagged_corpus = rule_tag_dict(
            rule_list, time_encoded_corpus, RULE_TAG_TABLE['quantifier'])

        # quantifier encoding
        quantifier_encoded_corpus = ne_encode(
            quantifier_tagged_corpus, RULE_TAG_TABLE['quantifier'])

        # write preprocessed file
        all_preprocessed_content = quantifier_encoded_corpus
        write_file(output_path, all_preprocessed_content)

    if arguments['re_train']:

        # active dict tagging
        dict_tagged_corpus = dict_tag_corpus(corpus, use_dict_priority='A')

        corpus_list = dict_tagged_corpus.split('\n')
        corpus_list = filter(None, corpus_list)
        pre_corpus_list = []

        for paragraph in corpus_list:
            start_sign_flag = False
            start_index = 0
            sent_list = []
            for i, ch in enumerate(paragraph):
                if ch == '':
                    continue
                if re.match(r'[@]', ch):
                    if start_sign_flag != True:
                        start_sign_flag = True
                        part_paragraph = paragraph[start_index:i]
                        paragraph_to_sent = split_sent(part_paragraph)
                        curr_sent_list = paragraph_to_sent.split('\n')
                        curr_sent_list = filter(None, curr_sent_list)
                        sent_list.extend(curr_sent_list)
                        start_index = i
                    else:
                        start_sign_flag = False
                        part_paragraph = paragraph[start_index:i+1]
                        sent_list.append(part_paragraph)
                        start_index = i+1
            part_paragraph = paragraph[start_index:]
            if part_paragraph != '':
                paragraph_to_sent = split_sent(part_paragraph)
                curr_sent_list = paragraph_to_sent.split('\n')
                curr_sent_list = filter(None, curr_sent_list)
                sent_list.extend(curr_sent_list)
                sent_list = filter(None, sent_list)
                sent_list = [sent.strip() for sent in sent_list]
            paragraph = '\n'.join(sent_list)
            pre_corpus_list.append(paragraph)
            pre_corpus_list.append('')

        pre_corpus = '\n'.join(pre_corpus_list)

        # dict tagging
        dict_tagged_corpus = dict_tag_corpus(pre_corpus, use_dict_priority='NOPW')

        # time tagging
        rule_path = os.path.join(RULE_FILES_PATH, 'time.rule')
        rule_list = read_file(rule_path).split('\n')
        time_tagged_corpus = rule_tag_dict(
            rule_list, dict_tagged_corpus, RULE_TAG_TABLE['time'])

        # time encoding
        time_encoded_corpus = ne_encode(
            time_tagged_corpus, RULE_TAG_TABLE['time'])

        # quantifier tagging
        rule_path = os.path.join(RULE_FILES_PATH, 'quantifier.rule')
        rule_list = read_file(rule_path).split('\n')
        quantifier_tagged_corpus = rule_tag_dict(
            rule_list, time_encoded_corpus, RULE_TAG_TABLE['quantifier'])

        # quantifier encoding
        quantifier_encoded_corpus = ne_encode(
            quantifier_tagged_corpus, RULE_TAG_TABLE['quantifier'])

        write_file(output_path, quantifier_encoded_corpus)


if __name__ == '__main__':
    main()
    print __file__, 'finish.'
