# coding=utf8

"""Postprocessing for Word Segmentation.

Usage:
    postprocessing.py tag_recovery <text_file> <output_file>
    postprocessing.py time_recovery <text_file> <output_file>
    postprocessing.py quantifier_recovery <text_file> <output_file>
    postprocessing.py all_postprocess <text_file> <output_file>
    postprocessing.py use_active_learning_result <text_file> <output_file>
    postprocessing.py (-h | --help)
    postprocessing.py --version

Options:
    -h --help   Show this screen.
    --version   Show this version.

"""

import re
import os
import json
import logging

from docopt import docopt


logging.basicConfig(level=logging.DEBUG, filename='log')
RULE_TAG_TABLE = {}
RULE_TAG_TABLE['time'] = 'T'
RULE_TAG_TABLE['quantifier'] = 'Q'
DICT_TAG_PATH = 'dict'
CHARSET = 'utf8'


def read_file(path):
    """ read a file. """
    if os.path.exists(path):
        with open(path, 'r') as corpus:
            return corpus.read().decode(CHARSET)
    else:
        print "{} could not found".format(path)
        exit(1)


def write_file(path, context):
    """ write a file. """
    context = context.encode(CHARSET)
    with open(path, 'w') as corpus:
        corpus.write(context)


def build_tag_dict(tag, tag_data):
    """ use letter and number to represent each name. """
    tag_data = filter(None, tag_data.split('\n'))
    tag_dict = {}
    for index, name in enumerate(tag_data):
        tag_dict[name] = tag + str(index)
    return tag_dict


def recovery_tag(tagged_corpus):
    """ recovery the name of tag. """

    for tag_file in os.listdir(DICT_TAG_PATH):
        tag = os.path.basename(tag_file)[0]
        tag_file_path = os.path.join(DICT_TAG_PATH, tag_file)
        tag_data = read_file(tag_file_path)
        tag_dict = build_tag_dict(tag, tag_data)
        reverse_tag_dict = dict((tag_no, tag_content)
                                for tag_content, tag_no in tag_dict.iteritems())
        pat = re.compile(r'@[^@]+@')
        for match in pat.findall(tagged_corpus):
            if tag in match:
                tag_no = match.replace('@', '')
                tag_no = tag_no.replace(' ', '')
                name = reverse_tag_dict[tag_no]
                tagged_corpus = tagged_corpus.replace(match, ' ' + name + ' ')

    return tagged_corpus


def recovery_rule_tag(tagged_corpus, tag_sign):
    """ recovery rule tag. """

    tag_sign_file = read_file(tag_sign)
    tag_sign_dict = json.loads(tag_sign_file)
    reverse_tag_sign_dict = dict((tag_no, tag_content)
                                 for tag_content, tag_no in tag_sign_dict.iteritems())

    pat = re.compile(r'{}[^{}]+{}'.format(tag_sign, tag_sign, tag_sign))
    for match in pat.findall(tagged_corpus):
        tag_no = match.replace(' ', '')
        tag_content = reverse_tag_sign_dict[tag_no]
        tag_content = tag_content.replace(tag_sign, '')
        tagged_corpus = tagged_corpus.replace(match, ' ' + tag_content + ' ')

    return tagged_corpus


def correct_corpus(active_learning_result, corpus):
    """ use active learning result to correct corpus """
    active_learning_result_list = active_learning_result.split('\n')
    for line in active_learning_result_list:
        if '=>' in line:
            line = line.split('=>')
            system_output = line[0]
            human_ouput = line[1]
            correct_pat = re.compile(system_output, re.MULTILINE)
            corpus = correct_pat.sub(human_ouput, corpus)
    return corpus

def main():
    """ turn tag number into name."""
    argments = docopt(__doc__, version="word segmentation postprocessing 1.0")
    corpus_path = argments['<text_file>']
    output_path = argments['<output_file>']
    corpus = read_file(corpus_path)
    if argments['tag_recovery']:
        recovery_tagged_corpus = corpus
        recovery_tagged_corpus = recovery_tag(recovery_tagged_corpus)
        write_file(output_path, recovery_tagged_corpus)

    if argments['time_recovery']:
        recovery_time_corpus = recovery_rule_tag(
            corpus, RULE_TAG_TABLE['time'])
        write_file(output_path, recovery_time_corpus)

    if argments['quantifier_recovery']:
        recovery_quantifier_corpus = recovery_rule_tag(
            corpus, RULE_TAG_TABLE['quantifier'])
        write_file(output_path, recovery_quantifier_corpus)

    if argments['all_postprocess']:
        # tag recovery
        recovery_tagged_corpus = recovery_tag(corpus)

        # time recovery
        recovery_time_corpus = recovery_rule_tag(
            recovery_tagged_corpus, RULE_TAG_TABLE['time'])

        # quantifier recovery
        recovery_quantifier_corpus = recovery_rule_tag(
            recovery_time_corpus, RULE_TAG_TABLE['quantifier'])

        # turn multiple space into one space
        all_postprocessed_list = filter(None, recovery_quantifier_corpus.split('\n'))
        for i in xrange(0, len(all_postprocessed_list)):
            line = all_postprocessed_list[i]
            line = re.sub(r'\s+', ' ', line)
            line = line.strip()
            all_postprocessed_list[i] = line
        all_postprocessed_content = '\n'.join(all_postprocessed_list)
        write_file(output_path, all_postprocessed_content)

    if argments['use_active_learning_result']:
        active_learning_result = read_file('prefix_suffix_word_list')
        corpus = correct_corpus(active_learning_result, corpus)
        write_file(output_path, corpus)

if __name__ == '__main__':
    main()
    print __file__, 'finish.'
